.SUFFIXES: .html .adoc

.adoc.html:
	asciidoctor $<

all: index.html datc.html mission.html

clean:
	rm -f index.html mission.html
