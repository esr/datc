#!/usr/bin/env python3
#
# A stupid little directory lister because GitLab doesn't want to serve
# directories without index files.  Deliberately retro.
#
import os,os.path,sys,time

def rfc3339(t):
    "RFC3339 string from Unix time."
    return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(t))

sys.stdout.write("""\
<!doctype html>
<body>
<pre>
""")
sys.stdout.write(open("README").read())
sys.stdout.write("""\
</pre>
<hr/>
<table border='0' width='50%'>
<tr><td><b>Name</b></td><td><b>Size</b></td><td><b>Last Modified</b></td></tr>
""")
subpaths = os.listdir(".")
subpaths.sort()
for path in subpaths:
    if path.endswith("index.html") or path == "README":
        continue
    size = os.path.getsize(path)
    mtime = rfc3339(os.path.getmtime(path))
    sys.stdout.write('<tr><td><a href="%s">%s</a></td></td><td>%d</td><td>%s</td></tr>\n' %(path, path, size, mtime))
sys.stdout.write("""\
</table>
</body>
</html>
""")
