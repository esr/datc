#!/usr/bin/env python3
#
# datgen - filter the jDip DATC test set into new DATC canonical form
#
# Run this with a collection of jDip test set files as command-line
# arguments.  DATC standard form for their concatenation will be
# emitted to standard output.
#
# This script is grubby and ad-hoc. At some point the test case language
# will reach 1.0 and the script will become a relic kept around to document how
# the cleanup was done.
#
# TODO: Parse for hasbro ("2000 rules") and strzykman options.
#       The relevant case is 6.F.17.
#
# The following jDip cases aren't described in DATC:
# 6.A.5.old
# 6.A.10.old
# 6.F.22.extended
# 6.G.10.mod
# 6.G.11.mod
# 6.H.5.mod
# Zach.A
# Zach.B
# 9.TEST.A
# 9.TEST.B
# 9.TEST.C
#
# SPDX-License-Identifier: BSD-2-Clause

import sys, re

explanation =  '''\
#############################################################
#
#	DATC (Diplomacy Adjudicator Test Cases)
#       Version 3.0 beta
#
# This is Lucas B. Kruijswijk's DATC test load in a version optimized
# for machine processing, with explicit postconditions added. All
# conventions used in this file are described in this header comment.
#
# Note: the test-set specification language should not yet be considered
# stable or final. The preceding disclaimer will be removed before the
# first public release is shipped.
#
# Credit where credit is due: the content (though not the syntax) is
# mostly from the jDip test set. Differences:
#
# 1. The entire test language is exactly documented.
# 2. Proper Diplomacy notation conformant to the 2000 edition is used every
#    place it can be, making life easier for order parsers to interpret the
#    tests.
# 3. Parsing is less context-dependent.  Every line has an action verb.
# 4. The notation is explicit about unit destruction.
# 5. Mixed use of short and long names for regions has been cleaned up.
# 6. Many small fixes have been applied to correct typos.
# 7. Consistent syntax for 'by/via Convoy' extension.
# 8. Fixed a missing END marker.
#
# SPDX-License-Identifier: BSD-2-Clause
#############################################################

DEX 1.0

MODULE Standard

# Regions:
terrain edge sea NwS
terrain edge sea Bar
terrain edge sea NAO
terrain coast Cly Nwy Edi
terrain sea Ska
terrain coast Fin
terrain sea NtS
terrain edge coast StP with nc sc
terrain coast Lvp Swe Yor Den
terrain sea Bot
terrain coast Wal
terrain sea Hel Bal Iri
terrain coast Lon Lvn
terrain edge land Mos
terrain coast Kie
terrain sea Cha
terrain coast Hol Ber Pru
terrain land War
terrain coast Bel
terrain land Sil
terrain coast Pic
terrain land Ruh Ukr
terrain coast Bre
terrain land Mun Boh Gal Bur
terrain edge sea MAO
terrain land Par Tyo Vie
terrain coast Rum
terrain edge coast Sev Gas
terrain land Bud
terrain coast Tri Mar Pie Ven
terrain sea Bla
terrain land Ser
terrain coast Tus
terrain sea Adr
terrain coast Alb
terrain coast Bul with ec sc
terrain coast Por
terrain coast Spa with nc sc
terrain sea GoL
terrain coast Con Ank Rom Gre
terrain edge coast Arm
terrain sea WMS Tyn
terrain coast Nap Apu
terrain sea Aeg
terrain coast Smy
terrain edge sea Ion
terrain edge coast NAf
terrain edge coast Tun
terrain edge sea EMS
terrain edge coast Syr

# Centers:
set center Nwy Edi StP Lvp Swe Den Lon Mos Kie Hol Ber War Bel Bre Mun Par Vie
set center Rum Sev Bud Tri Mar Ven Ser Bul Por Spa Con Ank Rom Gre Nap Smy Tun

# Mapped links:
link mapped sea connects NwS to Bar Nwy NtS Edi Cly NAO
link mapped sea connects Bar to nc StP Nwy NwS
link mapped sea connects NAO to NwS Cly Lvp Iri Iri MAO
link mapped sea connects Cly to NwS
link mapped coast connects Cly to Edi	# by sea NwS
link mapped coast connects Cly to Lvp	# by sea NAO
link mapped sea connects Cly to NAO
link mapped sea connects Nwy to NtS NwS Bar
link mapped coast connects Nwy to nc StP	# by sea Bar
link mapped land connects Nwy to Fin
link mapped coast connects Nwy to Swe	# by sea Ska
link mapped sea connects Nwy to Ska
link mapped sea connects Edi to NwS NtS
link mapped coast connects Edi to Yor	# by sea NtS
link mapped land connects Edi to Lvp
link mapped coast connects Edi to Cly	# by sea NwS
link mapped sea connects Ska to Nwy Swe Den NtS
link mapped coast connects Fin to sc StP	# by sea Bot
link mapped sea connects Fin to Bot
link mapped coast connects Fin to Swe	# by sea Bot
link mapped land connects Fin to Nwy
link mapped sea connects NtS to NwS Nwy Ska Den Hel Hol Bel Cha Lon Yor Edi
link mapped coast connects nc StP to Nwy	# by sea Bar
link mapped sea connects nc StP to Bar
link mapped land connects StP to Mos
link mapped coast connects sc StP to Lvn	# by sea Bot
link mapped sea connects sc StP to Bot
link mapped coast connects sc StP to Fin	# by sea Bot
link mapped sea connects Lvp to NAO
link mapped coast connects Lvp to Cly	# by sea NAO
link mapped land connects Lvp to Edi Yor
link mapped coast connects Lvp to Wal	# by sea Iri
link mapped sea connects Lvp to Iri
link mapped sea connects Swe to Ska
link mapped coast connects Swe to Nwy	# by sea Ska
link mapped coast connects Swe to Fin	# by sea Bot
link mapped sea connects Swe to Bot Bal
link mapped coast connects Swe to Den	# by sea Ska
link mapped coast connects Swe to Den	# by sea Bal
link mapped coast connects Yor to Edi	# by sea NtS
link mapped sea connects Yor to NtS
link mapped coast connects Yor to Lon	# by sea NtS
link mapped land connects Yor to Wal Lvp
link mapped sea connects Den to Ska
link mapped coast connects Den to Swe	# by sea Ska
link mapped coast connects Den to Swe	# by sea Bal
link mapped sea connects Den to Bal
link mapped coast connects Den to Kie	# by sea Bal
link mapped coast connects Den to Kie	# by sea Hel
link mapped sea connects Den to Hel NtS
link mapped sea connects Bot to Fin
link mapped sea connects Bot to sc StP Lvn Bal Swe
link mapped coast connects Wal to Lvp	# by sea Iri
link mapped land connects Wal to Yor
link mapped coast connects Wal to Lon	# by sea Cha
link mapped sea connects Wal to Cha Iri
link mapped sea connects Hel to Den Kie Hol NtS
link mapped sea connects Bal to Den Swe Bot Lvn Pru Ber Kie
link mapped sea connects Iri to NAO Lvp Wal Cha MAO NAO
link mapped coast connects Lon to Yor	# by sea NtS
link mapped sea connects Lon to NtS Cha
link mapped coast connects Lon to Wal	# by sea Cha
link mapped sea connects Lvn to Bot
link mapped coast connects Lvn to sc StP	# by sea Bot
link mapped land connects Lvn to Mos War
link mapped coast connects Lvn to Pru	# by sea Bal
link mapped sea connects Lvn to Bal
link mapped land connects Mos to StP Sev Ukr War Lvn
link mapped sea connects Kie to Hel
link mapped coast connects Kie to Den	# by sea Hel
link mapped coast connects Kie to Den	# by sea Bal
link mapped sea connects Kie to Bal
link mapped coast connects Kie to Ber	# by sea Bal
link mapped land connects Kie to Mun Ruh
link mapped coast connects Kie to Hol	# by sea Hel
link mapped sea connects Cha to Iri Wal Lon NtS Bel Pic Bre MAO
link mapped sea connects Hol to Hel
link mapped coast connects Hol to Kie	# by sea Hel
link mapped land connects Hol to Ruh
link mapped coast connects Hol to Bel	# by sea NtS
link mapped sea connects Hol to NtS
link mapped sea connects Ber to Bal
link mapped coast connects Ber to Pru	# by sea Bal
link mapped land connects Ber to Sil Mun
link mapped coast connects Ber to Kie	# by sea Bal
link mapped sea connects Pru to Bal
link mapped coast connects Pru to Lvn	# by sea Bal
link mapped land connects Pru to War Sil
link mapped coast connects Pru to Ber	# by sea Bal
link mapped land connects War to Lvn Mos Ukr Gal Sil Pru
link mapped sea connects Bel to Cha NtS
link mapped coast connects Bel to Hol	# by sea NtS
link mapped land connects Bel to Ruh Bur
link mapped coast connects Bel to Pic	# by sea Cha
link mapped land connects Sil to Ber Pru War Gal Boh Mun
link mapped sea connects Pic to Cha
link mapped coast connects Pic to Bel	# by sea Cha
link mapped land connects Pic to Bur Par
link mapped coast connects Pic to Bre	# by sea Cha
link mapped land connects Ruh to Hol Kie Mun Bur Bel
link mapped land connects Ukr to War Mos Sev Rum Gal
link mapped sea connects Bre to Cha
link mapped coast connects Bre to Pic	# by sea Cha
link mapped land connects Bre to Par
link mapped coast connects Bre to Gas	# by sea MAO
link mapped sea connects Bre to MAO
link mapped land connects Mun to Kie Ber Sil Boh Tyo Bur Ruh
link mapped land connects Boh to Sil Gal Vie Tyo Mun
link mapped land connects Gal to Sil War Ukr Rum Bud Vie Boh
link mapped land connects Bur to Pic Bel Ruh Mun Mar Gas Par
link mapped sea connects MAO to NAO Iri Cha Bre Gas
link mapped sea connects MAO to nc Spa Por
link mapped sea connects MAO to sc Spa WMS NAf
link mapped land connects Par to Bre Pic Bur Gas
link mapped land connects Tyo to Mun Boh Vie Tri Ven Pie
link mapped land connects Vie to Boh Gal Bud Tri Tyo
link mapped land connects Rum to Gal Ukr
link mapped coast connects Rum to Sev	# by sea Bla
link mapped sea connects Rum to Bla
link mapped coast connects Rum to ec Bul	# by sea Bla
link mapped land connects Rum to Ser Bud
link mapped land connects Sev to Ukr Mos
link mapped coast connects Sev to Arm	# by sea Bla
link mapped sea connects Sev to Bla
link mapped coast connects Sev to Rum	# by sea Bla
link mapped coast connects Gas to Bre	# by sea MAO
link mapped land connects Gas to Par Bur Mar
link mapped coast connects Gas to nc Spa	# by sea MAO
link mapped sea connects Gas to MAO
link mapped land connects Bud to Gal Rum Ser Tri Vie
link mapped land connects Tri to Tyo Vie Bud Ser
link mapped coast connects Tri to Alb	# by sea Adr
link mapped sea connects Tri to Adr
link mapped coast connects Tri to Ven	# by sea Adr
link mapped land connects Mar to Bur
link mapped coast connects Mar to Pie	# by sea GoL
link mapped sea connects Mar to GoL
link mapped coast connects Mar to sc Spa	# by sea GoL
link mapped land connects Mar to Gas
link mapped land connects Pie to Tyo Ven
link mapped coast connects Pie to Tus	# by sea GoL
link mapped sea connects Pie to GoL
link mapped coast connects Pie to Mar	# by sea GoL
link mapped land connects Ven to Tyo
link mapped coast connects Ven to Tri	# by sea Adr
link mapped sea connects Ven to Adr
link mapped coast connects Ven to Apu	# by sea Adr
link mapped land connects Ven to Rom Tus Pie
link mapped sea connects Bla to Rum Sev Arm Ank Con
link mapped sea connects Bla to ec Bul
link mapped land connects Ser to Tri Bud Rum Bul Gre Alb
link mapped coast connects Tus to Pie	# by sea GoL
link mapped land connects Tus to Ven
link mapped coast connects Tus to Rom	# by sea Tyn
link mapped sea connects Tus to Tyn GoL
link mapped sea connects Adr to Tri Alb Ion Apu Ven
link mapped coast connects Alb to Tri	# by sea Adr
link mapped land connects Alb to Ser
link mapped coast connects Alb to Gre	# by sea Ion
link mapped sea connects Alb to Ion Adr
link mapped coast connects ec Bul to Rum	# by sea Bla
link mapped sea connects ec Bul to Bla
link mapped coast connects ec Bul to Con	# by sea Bla
link mapped coast connects sc Bul to Con	# by sea Aeg
link mapped sea connects sc Bul to Aeg
link mapped coast connects sc Bul to Gre	# by sea Aeg
link mapped land connects Bul to Ser
link mapped coast connects Por to nc Spa	# by sea MAO
link mapped coast connects Por to sc Spa	# by sea MAO
link mapped sea connects Por to MAO
link mapped sea connects nc Spa to MAO
link mapped coast connects nc Spa to Gas	# by sea MAO
link mapped coast connects sc Spa to Mar	# by sea GoL
link mapped sea connects sc Spa to GoL
link mapped sea connects sc Spa to WMS
link mapped sea connects sc Spa to MAO
link mapped coast connects nc Spa to Por	# by sea MAO
link mapped coast connects sc Spa to Por	# by sea MAO
link mapped sea connects GoL to Mar Pie Tus Tyn WMS
link mapped sea connects GoL to sc Spa
link mapped sea connects Con to Bla
link mapped coast connects Con to Ank	# by sea Bla
link mapped coast connects Con to Smy	# by sea Aeg
link mapped sea connects Con to Aeg
link mapped coast connects Con to ec Bul	# by sea Bla
link mapped coast connects Con to sc Bul	# by sea Aeg
link mapped sea connects Ank to Bla
link mapped coast connects Ank to Arm	# by sea Bla
link mapped land connects Ank to Smy
link mapped coast connects Ank to Con	# by sea Bla
link mapped coast connects Rom to Tus	# by sea Tyn
link mapped land connects Rom to Ven Apu
link mapped coast connects Rom to Nap	# by sea Tyn
link mapped sea connects Rom to Tyn
link mapped coast connects Gre to Alb	# by sea Ion
link mapped land connects Gre to Ser
link mapped coast connects Gre to sc Bul	# by sea Aeg
link mapped sea connects Gre to Aeg Ion
link mapped coast connects Arm to Ank	# by sea Bla
link mapped sea connects Arm to Bla
link mapped coast connects Arm to Sev	# by sea Bla
link mapped land connects Arm to Syr Smy
link mapped sea connects WMS to sc Spa GoL Tyn Tun NAf MAO
link mapped sea connects Tyn to GoL Tus Rom Nap Ion Tun WMS
link mapped coast connects Nap to Rom	# by sea Tyn
link mapped coast connects Nap to Apu	# by sea Ion
link mapped sea connects Nap to Ion Tyn
link mapped land connects Apu to Rom
link mapped coast connects Apu to Ven	# by sea Adr
link mapped sea connects Apu to Adr Ion
link mapped coast connects Apu to Nap	# by sea Ion
link mapped sea connects Aeg to Gre
link mapped sea connects Aeg to sc Bul Con Smy EMS Ion
link mapped coast connects Smy to Con	# by sea Aeg
link mapped land connects Smy to Ank Arm
link mapped coast connects Smy to Syr	# by sea EMS
link mapped sea connects Smy to EMS Aeg
link mapped sea connects Ion to Adr Alb Gre Aeg EMS Tun Tyn Nap Apu
link mapped sea connects NAf to WMS
link mapped coast connects NAf to Tun	# by sea WMS
link mapped sea connects NAf to MAO
link mapped sea connects Tun to WMS Tyn Ion
link mapped coast connects Tun to NAf	# by sea WMS
link mapped sea connects EMS to Aeg Smy Syr Ion
link mapped coast connects Syr to Smy	# by sea EMS
link mapped land connects Syr to Arm
link mapped sea connects Syr to EMS

region Adr is "Adriatic Sea"
region Aeg is "Aegean Sea"
region Alb is "Albania"
region Ank is "Ankara"
region Apu is "Apulia"
region Arm is "Armenia"
region Bar is "Barents Sea"
region Bal is "Baltic Sea"
region Bel is "Belgium"
region Ber is "Berlin"
region Bla is "Black Sea"
region Boh is "Bohemia"
region Bot is "Gulf of Bothnia"
region Bre is "Brest"
region Bud is "Budapest"
region Bul is "Bulgaria"
region Bur is "Burgundy"
region Con is "Constantinople"
region Cha is "English Channel"	       	# DATC "Eng"
region Cly is "Clyde"
region Den is "Denmark"
region Edi is "Edinburgh"
region EMS is "Eastern Mediterranean" aka Eas # DATC alias
region Fin is "Finland"
region Gal is "Galicia"
region Gas is "Gascony"
region Gre is "Greece"
region Hel is "Heligoland Bight"
region Hol is "Holland"
region Ion is "Ionian Sea"
region Iri is "Irish Sea"
region Kie is "Kiel"
region GoL is "Gulf of Lions"	# Gulf of Lyons, incorrectly, in AH Diplomacy
region Lvp is "Liverpool"
region Lon is "London"
region Lvn is "Livonia"
region Mar is "Marseilles"
region MAO is "Mid-Atlantic" aka Mid	# DATC alias
region Mun is "Munich"
region Mos is "Moscow"
region NAf is "North Africa"
region Nap is "Naples"
region NAO is "North Atlantic" aka NAt	# DATC alias
region Nwy is "Norway"
region NwS is "Norwegian Sea" aka Nrg Nwg      # Nrg is DATC lias
region NtS is "North Sea" aka Nth	# DATC alias
region Par is "Paris"
region Pic is "Picardy"
region Pie is "Piedmont"
region Por is "Portugal"
region Pru is "Prussia"
region Rom is "Rome"
region Ruh is "Ruhr"
region Rum is "Rumania"
region Ser is "Serbia"
region Sev is "Sevastopol"
region Sil is "Silesia"
region Ska is "Skagerrak"
region Smy is "Smyrna"
region Spa is "Spain"
region StP is "St. Petersburg"
region Syr is "Syria"
region Swe is "Sweden"
region Tri is "Trieste"
region Tun is "Tunis"
region Tyo is "Tyrolia"	aka Tyr		# DATC alias
region Tyn is "Tyrrhenian Sea"
region Tus is "Tuscany"
region Ukr is "Ukraine"
region Vie is "Vienna"
region Ven is "Venice"
region Wal is "Wales"
region War is "Warsaw"
region WMS is "Western Mediterranean" aka Wes	# DATC alias
region Yor is "Yorkshire"
END

MODULE Standard
copy StandardMap
nation Aus is "Austria" including Vie Bud Tri Tyo Boh Gal
nation Eng is "England" including Lon Lvp Edi Cly Wal Yor
nation Fra is "France" including Par Bre Mar Pic Bur Gas
nation Ita is "Italy" including Rom Nap Ven Tus Pie Apu
nation Ger is "Germany" including Ber Mun Kie Ruh Pru Sil
nation Rus is "Russia" including Mos StP Sev War Ukr Lvn Fin
nation Tur is "Turkey" including Con Ank Smy Arm Syr
#post army Eng at Lvp
#post fleet Eng at Lon Edi
#post army Ger at Ber Mun
#post fleet Ger at Kie
#post army Rus at Mos War
#post fleet Rus at Sev StP(sc)
#post army Tur at Con Smy
#post fleet Tur at Ank
#post army Aus at Vie Bud
#post fleet Aus at Tri
#post army Ita at Rom Ven
#post fleet Ita at Nap
#post army Fra at Par Mar
#post fleet Fra at Bre
victory 18 centers
END

MODULE DATC_Loeb9
copy StandardMap

terrain sea Arc
terrain coast Ice Ire Crs Sar Sic 
remove Spa
terrain coast Nav Cat Cor
terrain land Mad
terrain land Sib Kaz

region Arc is "Arctic Ocean"
region Cat is "Catalonia"
region Cor is "Cordoba"
region Crs is "Corsica"
region Ice is "Iceland"
region Ire is "Ireland"
region Kaz is "Kazakz"
region Mad is "Madrid"
region Nav is "Navarre"
region Sar is "Sardinia"
region Sib is "Siberia"
region Sic is "Sicily"

link mapped sea connects Arc with NAO NwS Bar Sib
link mapped land connects Cat with Mad
link mapped sea connects Cat with WME
link mapped coast connects Cat with Cor Mad Gas Mar GoL
link mapped land conects Cor with Mad
link mapped sea connects Cor with WME MAO
link mapped coast connects Cor with Por Cat
link mapped land difficult connects Cor with NAf
link mapped sea connects Crs with GoL Tyn
link mapped sea connects Ice with MAO NwS
link mapped sea connects Ire with Iri NAO
link mapped land connects Kaz with Mos Sev Sib
link mapped land connects Mad with Por Nav Cat Cor
link mapped sea connects Nav with MAO
link mapped coast connects Nav with Gas Cat Por
link mapped sea connects Sar with GoL Tyn
link mapped land connects Sib with Mos Kaz
link mapped sea connects Sib with Arc Bar
link mapped coast connects Sib with StP(nc)
link mapped sea connects Sic with Tyn Ion
link mapped land difficult connects Sic with Nap

nation C-Spa is Mad Cor Por
nation C-Nor is Nor Den Swe

#post army Eng at Lvp
#post fleet Eng at Lon Edi
#post army Ger at Ber Mun
#post fleet Ger at Kie
#post army Rus at Mos War
#post fleet Rus at Sev StP(nc)
#post army Tur at Con Smy
#post fleet Tur at Ank
#post army Aus at Vie Bud
#post fleet Aus at Tri
#post army Ita at Rom Ven
#post fleet Ita at Nap
#post army Fra at Par Mar
#post fleet Fra at Bre
#post army C-Spa at Mad
#post fleet C-Spa at Cor Por
#post army C-Nor at Nor
#post fleet C-Nor at Den Swe

set center Pru Sil Ire Mad Cor
set ice Arc
victory 20 centers
END

MODULE DATC_ConvoyingCoastal
copy StandardMap
remove Den Swe
terrain isles Den Swe
# Restore standard links
link mapped sea connects Den with Ska
link mapped coast connects Den with Swe	# by sea Ska
link mapped coast connects Den with Swe	# by sea Bal
link mapped sea connects Den with Bal
link mapped coast connects Den with Kie	# by sea Bal
link mapped coast connects Den with Kie	# by sea Hel
link mapped sea connects Den with Hel NtS
link mapped sea connects Swe to Ska
link mapped coast connects Swe to Nwy	# by sea Ska
link mapped coast connects Swe to Fin	# by sea Bot
link mapped sea connects Swe to Bot Bal
link mapped coast connects Swe to Den	# by sea Ska
link mapped coast connects Swe to Den	# by sea Bal
# New one
link mapped sea connects Den with Ber
set center Den Swe
victory 18 centers
END

'''

# Original DATC sections to testcase names.
reference_map = {
    "6.A.1": ["not-neighbor"],
    "6.A.2": ["army-to-sea"],
    "6.A.3": ["fleet-to-land"],
    "6.A.4": ["same-sector-move"],
    "6.A.5": ["same-sector-convoy"],
    "6.A.6": ["not-owner"],
    "6.A.7": ["no-fleet-convoy"],
    "6.A.8": ["notneighbor"],
    "6.A.9": ["coast-follow"],
    "6.A.10": ["unreachable-support"],
    "6.A.11": ["simple-bounce"],
    "6.A.12": ["three-unit-bounce"],
    "6.B.1": ["ambiguous-coast"],
    "6.B.2": ["implied-coast"],
    "6.B.3": ["wrong-coast"],
    "6.B.4": ["fleet-support-to-other-coast"],
    "6.B.5": ["fleet-support-from-other-coast"],
    "6.B.6": ["support-cut-from-other-coast"],
    "6.B.7": ["fleet-support-no-coast"],
    "6.B.8": ["fleet-support-to-unspecified-coast"],
    "6.B.9": ["fleet-support-to-wrong-coast"],
    "6.B.10": ["fleet-support-with-wrong-coast"],
    "6.B.11": ["no-coast-change"],
    "6.B.12": ["army-coast-irrelevant"],
    "6.B.13": ["no-coastal-crawl"],
    "6.B.14": ["ambiguous-bicoastal-fleet-build"],
    "6.C.1": ["three-army-cycle"],
    "6.C.2": ["three-army-cycle-support"],
    "6.C.3": ["disrupted-three-army-cycle"],
    "6.C.4": ["three-army-cycle-attacked"],
    "6.C.5": ["three-army-cycle-convoy"],
    "6.C.6": ["conoy-swap"],
    "6.C.7": ["swap-bounce-fails"],
    "6.D.1": ["hold-support"],
    "6.D.2": ["hold-support-cut"],
    "6.D.3": ["move-hold-support"],
    "6.D.4": ["support-of-hold-supporter"],
    "6.D.5": ["hold-support-to-move-supporter"],
    "6.D.6": ["hold-support-to-convoyer"],
    "6.D.7": ["hold-support-to-mover"],
    "6.D.8": ["hold-support-disrupted"],
    "6.D.9": ["holder-support-for-move"],
    "6.D.10": ["self-dislodge"],
    "6.D.11": ["self-dislodge-returning"],
    "6.D.12": ["assist-foreign-self-dislodge"],
    "6.D.13": ["foreign-self-dislodge-returning"],
    "6.D.14": ["spoiler-support"],
    "6.D.15": ["self-dislodge-cut"],
    "6.D.16": ["foreign-convoy-dislodge"],
    "6.D.17": ["dislodgment-cuts-support"],
    "6.D.18": ["survivor-sustains-support"],
    "6.D.19": ["survivor-sustains-support-alternate"],
    "6.D.20": ["perverse-support-cut"],
    "6.D.21": ["dislodged-support-cut"],
    "6.D.22": ["impossible-fleet-support"],
    "6.D.23": ["impossible-coast-support"],
    "6.D.24": ["impossible-move-support"],
    "6.D.25": ["failing-hold-support-supportable"],
    "6.D.26": ["failing-move-support-supportable"],
    "6.D.27": ["failing-convoy-supportable"],
    "6.D.28": ["impossible-move-and-support"],
    "6.D.29": ["impossible-bicoastal-move-support"],
    "6.D.30": ["no-coast-move-support"],
    "6.D.31": ["tricky-impossible-support"],
    "6.D.32": ["missing-fleet"],
    "6.D.33": ["unwanted-support"],
    "6.D.34": ["support-targeting-own-area"],
    "6.E.1": ["dislodged-no-effect"],
    "6.E.2": ["head-to-head-self-dislodge"],
    "6.E.3": ["foreign-assist-dislodge"],
    "6.E.4": ["non-dislodged-user"],
    "6.E.5": ["other-dislodged-user"],
    "6.E.6": ["foreign-help-non-dislodged-user"],
    "6.E.7": ["no-self-dislodge-beleaguered"],
    "6.E.8": ["no-self-dislodge-beleaguered-head-to-head"],
    "6.E.9": ["no-self-dislodge-beleaguered-moving"],
    "6.E.10": ["no-self-dislodge-beleaguered-cycle"],
    "6.E.11": ["beleaguered-unit-swap"],
    "6.E.12": ["own-support-attack"],
    "6.E.13": ["triple-beleaguered"],
    "6.E.14": ["illegal-still-defends"],
    "6.E.15": ["friendly-head-to-head"],
    "6.F.1": ["no-coastal-convoy"],
    "6.F.2": ["convoyed-army-normal-bounce"],
    "6.F.3": ["convoyed-army-support"],
    "6.F.4": ["convoy-disruption-by-attack"],
    "6.F.5": ["beleaguered-convoy"],
    "6.F.6": ["disrupted-convoy-support"],
    "6.F.7": ["disrupted-convoy-mo-contest"],
    "6.F.8": ["disrupted-convoy-no-bounce"],
    "6.F.9": ["multiroute-convoy-dislodge"],
    "6.F.10": ["unwanted-foreign-multiroute-convoy"],
    "6.F.11": ["multiroute-foreign-convoy-dislodge"],
    "6.F.12": ["convoy-dislodge-off-route"],
    "6.F.13": ["multiroute-convoy-alternative"],
    "6.F.14": ["simple-convoy-paradox"],
    "6.F.15": ["simple-convox-paradox-with-additional"],
    "6.F.16": ["pandins-paradox"],
    "6.F.17": ["pandins-extended-paradox"],
    "6.F.18": ["convoy-betrayal-paradox"],
    "6.F.19": ["multiroute-convoy-paradox"],
    "6.F.20": ["unwanted-multiroute-convoy-paradox"],
    "6.F.21": ["dads-army-convoy"],
    "6.F.22": ["second-order-convoy-paeadox"],
    "6.F.23": ["second-order-paradox-exclusive"],
    "6.F.24": ["second-order-paradox-noresolve"],
    "6.G.1": ["swap-by-convoy"],
    "6.G.2": ["army-kidnapping-convoy"],
    "6.G.3": ["kidnapping-with-disrupted-convoy"],
    "6.G.4": ["kidnapping-disrupted-with-opposite"],
    "6.G.5": ["convoy-swapping-with-intent"],
    "6.G.6": ["swap-with-unintended-convoy"],
    "6.G.7": ["swap-with-illegal-convoy"],
    "6.G.8": ["explicit-convoyer-missing"],
    "6.G.9": ["swap-or-dislodge"],
    "6.G.10": ["swapped-or-head-to-head"],
    "6.G.11": ["convoy-to-adjacent-with-paradox"],
    "6.G.12": ["swap-units-with-two-convoys"],
    "6.G.13": ["support-cut-on-self-attack-via-convoy"],
    "6.G.14": ["bounce-by-convoy-to-adjacent-place"],
    "6.G.15": ["bounce-and-dislodge-with-double-convoy"],
    "6.G.16": ["two-unit-in-one-area-convoy"],
    "6.G.17": ["two-unit-in-one-area-overland"],
    "6.G.18": ["two-unit-in-one-area-double-convoy"],
    "6.H.1": ["no-supports-during-retreat"],
    "6.H.2": ["no-supports-from-retreating-unit"],
    "6.H.3": ["no-convoy-during-retreat"],
    "6.H.4": ["no-other-moves-during-retreat"],
    "6.H.5": ["no-retreat-to-attack-source"],
    "6.H.6": ["no-retreat-to-contested-area"],
    "6.H.7": ["multiple-retreat-to-same-area"],
    "6.H.8": ["triple-retreat-to-same-area"],
    "6.H.9": ["dislodged-unit-does-not-contest"],
    "6.H.10": ["cannot-retreat-to-attack-origin"],
    "6.H.11": ["retreat-to-adjacent-convoy-origin"],
    "6.H.12": ["double-convoy-retreat-to-same-area"],
    "6.H.13": ["no-retreat-with-stale-convoy"],
    "6.H.14": ["no-main-phase-support-on-retreat"],
    "6.H.15": ["no-coastal-crawl-retreat"],
    "6.H.16": ["both-coasts-contested-on-retreat"],
    "6.I.1": ["too-many-builds"],
    "6.I.2": ["fleet-build-land-area"],
    "6.I.3": ["no-build-where-unit"],
    "6.I.4": ["no-multiple-build-on-multicoast"],
    "6.I.5": ["no-build-on-unowned-center"],
    "6.I.6": ["no-build-on-non-home-center"],
    "6.I.7": ["one-build-per-center"],
    "6.J.1": ["too-many-disbands"],
    "6.J.2": ["removing-same-unit-twice"],
    "6.J.3": ["civil-disorder-armies-different-distance"],
    "6.J.4": ["civil-disorder-armies-same-distance"],
    "6.J.5": ["civil-disorder-fleets-different-distance"],
    "6.J.6": ["civil-disorder-fleets-same-distance"],
    "6.J.7": ["civil-disorder-fleet-army-same-distance"],
    "6.J.8": ["civil-disorder-army-nearer"],
    "6.J.9.part1": ["civil-disorder-count-both-coasts-sc"],
    "6.J.9.part2": ["civil-disorder-count-both-coasts-nc"],
    "6.J.10": ["civil-disorder-count-convoying"],
    "6.J.11": ["civil-disorder-without-convoying"],
    "8.A": ["ice-to-move"],
    "8.B": ["ice-from-move"],
    "8.C": ["ice-from-support"],
    "8.D": ["ice-noretreat"],
    "8.E": ["ice-noconvoy"],
    "9.A": ["isles-no-convoy-dislodge"],
    "9.B": ["isles-no-convoy-loop"],
    "9.C": ["isles-army-disrupts-convoy"],
    "9.D": ["isles-convoy-disrupted-by-convoyed-army"],
    "9.E": ["isles-two-disrupted-convoy-paradox"],
    "9.F": ["isles-disrupted-convoy-paradox-no-resolution"],
    "9.G": ["isles-disrupted-convoy-paradox-two-resolutions"],
    "10.A": ["difficult-border-unsupported-no-cut"],
    "10.B": ["difficult-border-unsupport-cuts"],
    "10.C": ["difficult-border-no-bounce"],
    "10.D": ["difficult-border-two-bounce"],
    "10.E": ["difficult-border-no-support"],
    "10.F": ["difficult-border-support-paradox"],
    "10.G": ["difficult-border-almost-paradox-cordoba"],
    "10.G2": ["difficult-border-almost-paradox-sicily"],
    "10.H": ["difficult-border-circular"],
    "10.I": ["difficult-border-circular-bounce"],
    "10.J": ["difficult-border-circular-no-disrupt"],
    "10.K": ["difficult-border-retreat"],
    "10.L": ["difficult-border-property"],
    "10.M": ["difficult-border-convoy"],
    "10.N": ["difficult-border-convoy-bypass"],
    "10.O": ["difficult-border-self-attack-support"],
    "11.A": ["open-build"],
    "12.A": ["home-capture-unowned-center"],
    "12.B": ["home-capture-owned-center"],
    "12.C": ["home-capture-non-home"],
    "12.D": ["home-capture-civil-disorder"],
    # These were not in DATC 2.5
    "6.A.3.fleet.support.inland": ["fleet-support-inland"],
    "6.A.7.modified": ["no-fleet-convoy-honest"],
    }

pre_fixups = (
    ("\tGermnay:", "Germany:"),
    ("\tItaly ", "Italy: "),
    ("\tFrance ", "France: "),
    (" s ", " S "),
    (" Supports ", " S "),
    (" SUPPORTS ", " S "),
    (" hold", " H"),
    (" Hold", " H"),
    (" HOLD", " H"),
    (" c ", " C "),
    (" convoys ", " C "),
    (" Convoys ", " C "),
    (" CONVOYS ", " C "),
    (" f ", " F "),
    ("# Italy: the Dislodged prussian ", "### Italy: A Pru\t# The dislodged "),
    ("london not cut, fleet in eng", "Lon not cut, fleet in Ech"),
    ("\t# F NWG removed, as it is farther from supply center at stp", "\t### F Rus Nrg\t# Farther from supply center at StP"),
    ("\t# A Prussia removed, as it is farther from supply center at Naples", "\t### A C-Ber Pru\t # Farther from supply center at Naples"),
    ("\t# Russia:  A Sweden\t -- this unit is destroyed (not dislodged)",
     "\t### F Rus Swe\t# This unit is destroyed (not dislodged"),
    )

case_typos = {
    "6.F.E": "6.F.5",
    "14.L": "10.L",
    }

country_map = (
    ("Austria", "Aus"),
    ("England", "Eng"),
    ("France", "Fra"),
    ("Germany", "Ger"),
    ("Italy", "Ita"),
    ("Russia", "Rus"),
    ("Turkey", "Tur"),
    ("Norway", "C-Nor"),	# The ice variant
    ("Spain", "C-Spa"),		# Loeb9
    ("C-Ber", "C-Ber"),		# Loeb9
    )

province_map = (
    ("Adriatic Sea", "Adr"),
    ("Aegean Sea", "Aeg"),
    ("Albania", "Alb"),
    ("Ankara", "Ank"),
    ("Apulia", "Apu"),
    ("Armenia", "Arm"),
    ("Barents Sea", "Bar"),
    ("Baltic Sea", "Bal"),
    ("Belgium", "Bel"),
    ("Berlin", "Ber"),
    ("Black Sea", "Bla"),
    ("Bohemia", "Boh"),
    ("Gulf of Bothnia", "Bot"),
    ("Brest", "Bre"),
    ("Budapest", "Bud"),
    ("Bulgaria", "Bul"),
    ("Burgundy", "Bur"),
    ("Constantinople", "Con"),
    ("English Channel", "Eng"),
    ("Clyde", "Cly"),
    ("Denmark", "Den"),
    ("Edinburgh", "Edi"),
    ("Eastern Mediterranean", "Eas"),
    ("Finland", "Fin"),
    ("Galacia", "Gal"),		# Somebody's typo
    ("Galicia", "Gal"),
    ("Gascony", "Gas"),
    ("Greece", "Gre"),
    ("Heligoland Bight", "Hel"),
    ("Helgoland Bight", "Hel"),	# Somebody's typo
    ("Holland", "Hol"),
    ("Ionian Sea", "Ion"),
    ("Irish Sea", "Iri"),
    ("Kiel", "Kie"),
    ("Gulf of Lions", "GoL"),
    ("Liverpool", "Lvp"),
    ("London", "Lon"),
    ("Livonia", "Lvn"),
    ("Marseilles", "Mar"),
    ("Mid-Atlantic", "Mid"),
    ("Munich", "Mun"),
    ("Moscow", "Mos"),
    ("North Africa", "NAf"),
    ("Naples", "Nap"),
    ("North Atlantic", "NAt"),
    ("Norway", "Nwy"),
    ("Norwegian Sea", "Nrg"),
    ("North Sea", "Nth"),
    ("Paris", "Par"),
    ("Picardy", "Pic"),
    ("Piedmont", "Pie"),
    ("Portugal", "Por"),
    ("Prussia", "Pru"),
    ("Rome", "Rom"),
    ("Ruhr", "Ruh"),
    ("Rumania", "Rum"),
    ("Serbia", "Ser"),
    ("Sevastopol", "Sev"),
    ("Sicily", "Sic"),		# Loeb9 variant
    ("Silesia", "Sil"),
    ("Skagerrak", "Ska"),
    ("Smyrna", "Smy"),
    ("Spain", "Spa"),
    ("St. Petersburg", "StP"),
    ("Syria", "Syr"),
    ("Sweden", "Swe"),
    ("Trieste", "Tri"),
    ("Tunis", "Tun"),
    ("Tyrolia", "Tyr"),
    ("Tyrrhenian Sea", "Tyn"),
    ("Tuscany", "Tus"),
    ("Ukraine", "Ukr"),
    ("Vienna", "Vie"),
    ("Venice", "Ven"),
    ("Wales", "Wal"),
    ("Warsaw", "War"),
    ("Western Mediterranean", "Wes"),
    ("Yorkshire", "Yor"),
)

post_fixups = (
    ("ech", "ECh"),		# Not conformant to 2000 rules.
    ("- Eng", "- ECh"),		# Not conformant to 2000 rules.
    ("Eng -", "ECh -"),		# Not conformant to 2000 rules.
    ("F Eng Eng", "F Eng ECh"),	# Not conformant to 2000 rules.
    ("gob", "Bot"),
    ("Gol", "GoL"),
    ("mao", "Mid"),
    ("naf", "NAf"),
    ("nao", "NAt"),
    ("nwg", "Nrg"),
    (" nor", " Nwy"),
    ("Stp", "StP"),
    ("tys", "Tyn"),
    ("wal", "Wal"),
    ("via Convoy", "by convoy"),
    ("by Convoy", "by convoy"),
    ("A Eng nor", "A Eng Nwy"),
    ("F Rus Baltic", "F Rus Bal"),
    ("nth-Nrg successful", "Nth-Nrg successful"),
    ("norway-nth successful", "Nwy-Nth successful"),
    ("like 6.e.9, but fleet is moving", "like 6.E.8, but fleet is moving"),
    ("eleagured", "eleaguered"),
    ("Fra: Remove GoL", "F Fra GoL disband"),
    ("Fra: Remove Pic", "A Fra Pic disband"),
    ("Fra: Remove Par", "A Fra Par disband"),
    ("Fra: Remove A Par", "A Fra Par disband"),
    ("Nwymal", "normal"),
    )

noend = ["10.K"]

dashprotect = re.compile(r"C-([A-Z])")
dashpart = re.compile(r"([a-zA-Z)])-([a-zA-Z])")

placed_units = []
dislodged_units = ""

def killercomment(txt):
    if txt is None:
        return False
    return ("disband" in txt) or ("destroy" in txt) or ("remove" in txt)

def process(state, line, comment, path, ln):
    global dislodged_units
    for (longname, shortname) in country_map:
        line = dashprotect.sub(r"C%\1", line)
        line = dashpart.sub(r"\1 - \2", line)
        line = line.replace("C%", "C-")
        line = line.replace(" a ", " A")
        line = line.replace(longname + ":", shortname + ":")
        line = re.sub(shortname + r": +([AF])", r"\1 " + shortname, line)
        line = re.sub(shortname + r": +Build ([AF])", r" build \1 " + shortname, line)
        line = re.sub(shortname + r": +Disband ([AF])", r" disband \1 " + shortname, line)
    for (longname, shortname) in province_map:
        line = line.replace(" " + shortname.lower(), " " + shortname.capitalize())
        line = line.replace("-" + shortname.lower(), "-" + shortname.capitalize())
        line = line.replace(longname, shortname)
        line = line.replace(longname.lower(), shortname)
    for item in post_fixups:
        line = line.replace(item[0], item[1])
    if state == "PLACE":
        global placed_units
        placed_units.append(" ".join(line.lstrip().split()[:3]))
    if state == "DISLODGED":
        gen = line.rstrip().replace("\t", "DESTROYED ")
        #sys.stderr.write("Auto-dislodge: " + repr(gen) + "\n")
        dislodged_units += gen + "\n"
    #if state == "ORDER" and killercomment(comment):
    #    dislodged_units += "DESTROYED " + line
    if state in ("PLACE", "ORDER", "DISLODGED", "EXISTS", "DESTROYED", "FAILURE", "SUCCESS"):
        return state + " " + line.lstrip()
    if state == "PRESTATE_SUPPLYCENTER_OWNERS":
        line = line.strip()
        if not (line.startswith("A ") or line.startswith("F ")):
            sys.stderr.write("%d: ownership line %s in wrong format\n" % (ln, line))
            raise SystemExit(1)
        return "ASSIGN " + line[2:] + "\n"
    # More processing is needed
    sys.stderr.write("%s:%d: error in state %s on line %r\n" % (path, ln, state, line))
    raise SystemExit(1)

def process_file(fp):
    global dislodged_units
    expect = None
    comments = ""
    casetag = None
    ln = 0
    comment = None
    variant_latch = None
    resolve_latch = False
    poststate_latch = False
    phase = None
    case = None

    def out_with_comment(text, comment):
        if comment == None:
            sys.stdout.write(text)
        else:
            for item in post_fixups:
                comment = comment.replace(item[0], item[1])
            sys.stdout.write(text.rstrip() + comment)

    for line in fp:
        if line == "# fixed prestate\n":
            continue
        if ("# POSTSTATE_DISLODGED" in line or "dislodged units are disbanded" in line):
            sys.stdout.write(dislodged_units)
            continue
        for item in pre_fixups:
            line = line.replace(item[0], item[1])
        ln += 1
        if not line.strip() and poststate_latch and case in noend:
            sys.stdout.write("END end::" + tag + "[]\n")
            poststate_latch = False
        if line.startswith("#") or not line.strip():
            for item in post_fixups:
                line = line.replace(item[0], item[1])
            sys.stdout.write(line.strip() + "\n")
            continue
        comment = None
        if "#" in line:
            cstart = line.rindex("#")
            if line[cstart-1].isspace():
                while cstart > 0 and line[cstart-1].isspace():
                    cstart -= 1
            comment = line[cstart:]
            line = line[:cstart] + "\n"
        if line.startswith("CASE"):
            global placed_units
            placed_units = []
            dislodged_units = ""
            case = line.split()[1]
            resolve_latch = False
            poststate_latch = False
            line = line.rstrip()
            fields = line.split(" ")
            #sys.stderr.write(repr(fields)+"\n")
            if fields[1].endswith("."):
                fields[1] = fields[1][:-1]
            if len(fields) > 2:
                if not fields[2].startswith("["):
                    fields[2] = "[" + fields[2]
                if not fields[-1].endswith("]"):
                    fields[-1] = fields[-1] + "]"
            fields[1] = case_typos.get(fields[1], fields[1])
            if fields[1] in reference_map:
                fields[1] = reference_map[fields[1]][0]
            else:
                sys.stderr.write("Unknown case ID %r\n" % fields[1])
            tag = fields[1]
            # This is where we turn the ID into an asciidoctor section tag
            fields[1] = "tag::" + tag + "[]"
            line = " ".join(fields[:2]) 
            out_with_comment(line + "\n", comment)
            continue
        elif line.startswith("END"):
            # This is where we make an asciidoctor end-section tag
            line = line.replace("END", "END end::" + tag + "[]")
            if not line.endswith("\n"):
                line += "\n"
            out_with_comment(line, comment)
            continue
        elif line.startswith("PRESTATE_SETPHASE") and line.endswith("Movement\n"):
            if variant_latch is not None:
                sys.stdout.write("VARIANT %s\n" % variant_latch)
            out_with_comment("PHASE MOVEMENT" + line[17:-11]  +  " \n", comment)
            phase = "MOVEMENT"
            continue
        elif line.startswith("PRESTATE_SETPHASE") and line.endswith("Retreat\n"):
            if variant_latch is not None:
                sys.stdout.write("VARIANT %s\n" % variant_latch)
            out_with_comment("PHASE RETREAT" + line[17:-10]  + " \n", comment)
            phase = "RETREAT"
            continue
        elif line.startswith("PRESTATE_SETPHASE") and line.endswith("Adjustment\n"):
            if variant_latch is not None:
                sys.stdout.write("VARIANT %s\n" % variant_latch)
            out_with_comment("PHASE ADJUSTMENT" + line[17:-13] +  "\n", comment)
            phase = "ADJUSTMENT"
            continue
        elif line.startswith("VARIANT_ALL"):
            if line.strip() != "VARIANT_ALL Standard":
                variant_latch = line.strip().split()[1]
            continue
        elif line.startswith("VARIANT"):
            out_with_comment(line, comment)
            variant_latch = None
            continue
        elif line.strip() == "PRESTATE":
            expect = "PLACE"
            continue
        elif line.strip() in ("ORDERS", "Orders:", "Orders"):
            expect = "ORDER"
            continue
        elif line.strip() == "POSTSTATE":
            expect = "EXISTS"
            if not resolve_latch:
                out_with_comment("RESOLVE\n", comment)
                resolve_latch = True
            poststate_latch = True
            continue
        elif line == "PRESTATE_DISLODGED\n":
            expect = "DISLODGED"
            continue
        elif line.strip() == "POSTSTATE_DISLODGED":
            if not resolve_latch:
                out_with_comment("RESOLVE\n", comment)
                resolve_latch = True
            expect = "DISLODGED"
            continue
        elif line.strip() == "PRESTATE_SUPPLYCENTER_OWNERS":
            expect = "PRESTATE_SUPPLYCENTER_OWNERS"
            continue
        elif line.strip() == "POSTSTATE_SAME":
            if not resolve_latch:
                out_with_comment("RESOLVE\n", comment)
                resolve_latch = True
            out_with_comment("# No changes\n", comment)
            for unit in placed_units:
                sys.stdout.write("EXISTS " + unit + "\n")
            continue
        elif line.strip() == "PRESTATE_RESULTS":
            if comment is not None:
                sys.stdout.write(comment.lstrip())
            continue
        elif line.lstrip().startswith("### ") and resolve_latch:
            expect = "DESTROYED"
            line = line[4:]
        elif line.lstrip().startswith("#") and ("DESTROYED" in comment.upper() or "REMOVED" in comment.upper()):
            expect = "DESTROYED"
            line = line.lstrip()[2:]
        elif line.lstrip().startswith("FAILURE: "):
            expect = "FAILURE"
            line = line[10:]
            # Fall through
        elif line.lstrip().startswith("SUCCESS: "):
            expect = "SUCCESS"
            line = line[10:]
            # Fall through
        elif line == "\n" and comment:
            sys.stdout.write(comment)
            continue
        elif line.startswith("\t#"):
            sys.stdout.write(comment.lstrip())
            continue
        elif not ":" in line:
            sys.stderr.write("%s:%d: unexpected directive: %r\n" % (fp.name, ln, line))
            raise SystemExit(1)
        elif line.strip().split(":")[0] not in dict(country_map):
            sys.stderr.write("%s:%d: ill-formed order line: %r\n" % (fp.name, ln, line))
            raise SystemExit(1)
        if expect == "DESTROYED" and comment.strip().startswith("# DESTROYED"):
            comment = None
        # We've handled everying we can with simple substitions
        out_with_comment(process(expect, line, comment, fp.name, ln), comment)

if __name__ == '__main__':
    sys.stdout.write(explanation)
    for arg in sys.argv[1:]:
        with open(arg) as fp:
            process_file(fp)

    sys.stdout.write("### END\n")
# end
