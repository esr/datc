#!/usr/bin/env python3
"""
A parser for the DATC test set

Gets and deserializes test cases from an input stream for use, e.g.,
in resolver unit tests. Expected usage pattern is, e.g.

        for case in CaseFactory(sys.stdin):
            run_this_test(case)

"""
# Brought to you by ESR, April 2019
# SPDX-License-Identifier: BSD-2-Clause

class Case(object):
    def __init__(self):
        self.name = None
        self.phase = None
        self.calendar = None
        self.variant = None
        self.assignments = []
        self.placements = []
        self.orders = []
        self.exists = []
        self.dislodged = []
        self.destroyed = []
        self.failures = []
        self.successes = []
    def validate(self):
       return self.case is not None \
              and self.phase is not None \
              and (len(self.assignments) + len(self.placements)) \
              and len(self.orders) \
              and (len(self.exists) + len(self.dislodged) + len(self.destroyed)) 
    def str(self):
        "Canonicalized dump without comments. Mainly for debugging"
        out = "CASE %s\n" % self.case
        out += "PHASE %s %s\n" % (self.phasem, elf.calendar)
        if self.variant is not None:
            out += "VARIANT %s\n"
        for item in self.assignments:
            out += "ASSIGN %s\n"
        for item in self.placements:
            out += "PLACE %s\n"
        for item in self.orders:
            out += "ORDER %s\n"
        out += "RESOLVE\n"
        for item in self.exists:
            out += "EXISTS %s\n"
        for item in self.dislodged:
            out += "DISLODGED %s\n"
        for item in self.destroyed:
            out += "DESTROYED %s\n"
        for item in self.successes:
            out += "EXISTS %s\n"
        for item in self.failures:
            out += "FAILURES %s\n"
        return out + "END\n"

class CaseFactoryException(BaseException):
    def __init__(self, msg, source, line, lineno):
        self.message = msg
        self.source = source
        self.line = line
        self.lineno = lineno
    def __str__(self):
        return "%s:%d: %s is %r\n" % \
               (self.source, self.lineno, self.message, self.line)

class CaseFactory(object):
    "An iterator that parses DATC cases from an input source."
    def __init__(self, stream):
        self.stream = stream
        self.pushback = None
        self.rawline = None
        self.lineno = 0

    @staticmethod
    def stripcomment(line):
        if line[0] == "#":
            return "\n"
        if "#" in line:
            cstart = line.rindex("#")
            if line[cstart-1].isspace():
                while cstart > 0 and line[cstart-1].isspace():
                    cstart -= 1
            comment = line[cstart:]
            line = line[:cstart] + "\n"
        return line

    @staticmethod
    def equals(a, b):
        return a.upper() == b

    def readline(self):
        if self.pushback:
            passout = saved 
            self.pushback = None
            return CaseFactory.stripcomment(passout)
        nxt = self.stream.readline()
        # Can be useful to uncomment this for debugging
        #sys.stdout.write(repr(nxt)+"\n")
        if nxt == '':
            return ''
        self.rawline = nxt
        self.lineno += 1
        return CaseFactory.stripcomment(nxt)

    def pushline(self, line):
        "Not yet used"
        self.pushback = line

    def error(self, msg):
        raise CaseFactoryException(msg, self.stream.name,
                                   self.rawline, self.lineno)

    def __iter__(self):
        state = "ground"
        instance = Case()
        while True:
            line = self.readline()
            if line == '':
                return
            if line == "\n":
                continue
            fields = line.strip().split()
            if state == 'ground':
                # FIXME: someday use this to drive the grammar
                if line.startswith("DEX"):
                    continue
                elif CaseFactory.equals(fields[0], "CASE"):
                    instance.name = fields[1]
                    instance.phase = "MOVEMENT"
                    state = "precondition"
                else:
                    self.error("unexpected line while expecting CASE")
            elif state == "precondition":
                if CaseFactory.equals(fields[0], "VARIANT"):
                    instance.variant = fields[1]
                elif CaseFactory.equals(fields[0], "PHASE"):
                    instance.phase = fields[1]
                    instance.calendar = " ".join(fields[2:])
                elif CaseFactory.equals(fields[0], "PLACE"):
                    instance.placements.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "ASSIGN"):
                    instance.assignments.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "ORDER"):
                    instance.orders.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "DISLODGED"):
                    instance.dislodged.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "SUCCESS"):
                    instance.successes.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "FAILURE"):
                    instance.failures.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "RESOLVE"):
                    state = "postcondition"
                else:
                    self.error("unexpected line in precondition")
            elif state == "postcondition":
                if CaseFactory.equals(fields[0], "EXISTS"):
                    instance.exists.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "DISLODGED"):
                    instance.dislodged.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "DESTROYED"):
                    instance.destroyed.append(" ".join(fields[1:]))
                elif CaseFactory.equals(fields[0], "END"):
                    state = "ground"
                    yield instance
                else:
                    self.error("unexpected line in postcondition")

if __name__ == '__main__':
    # Smoke test for the parser
    import sys
    try:
        for case in CaseFactory(sys.stdin):
            pass
    except CaseFactoryException as e:
        sys.stderr.write(str(e))
